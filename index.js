var resizeTimer = setTimeout(function(){}, 1); // utility timer

var dimensions = {
    cached: {
        width: document.documentElement.clientWidth,
        height: document.documentElement.clientHeight
    },
    get width() {
        return dimensions.cached.width = document.documentElement.clientWidth;
    },
    get height() {
        return dimensions.cached.height = document.documentElement.clientHeight;
    }
}

var triggerResizeEnd = function(type) {
    if(!type) return;
    var event = new CustomEvent(type, {bubbles: true});
    document.body.dispatchEvent(event);
}

var maybeTriggerResizeEnd = function() {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
        if(dimensions.cached.width !== dimensions.width) {
            triggerResizeEnd('resizeEnd.zion');
            triggerResizeEnd('width.resizeEnd.zion');
        }
        if(dimensions.cached.width !== dimensions.width) {
            triggerResizeEnd('resizeEnd.zion');
            triggerResizeEnd('height.resizeEnd.zion');
        }
    }, 200);
}

window.addEventListener('resize', maybeTriggerResizeEnd);
